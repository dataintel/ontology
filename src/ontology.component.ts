import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
declare var vis: any;

export const ontologyComponentOptions = {
  selector: 'ontology',
  template: `<div id=mynetwork style="display: block; width: 500px; height: 300px;"></div>`,
  style: [`#mynetwork {
                box-sizing: border-box;
                border: 1px solid lightgray;
                width: 100%;
                height: 100%;
            }`]
};

@Component(ontologyComponentOptions)

export class BaseWidgetOntologyComponent implements OnInit, OnChanges {

  // @Input() dataOntology: any = data.data;
  @Input() dataOntology: any = {
    node: [
      {
        id: 0,
        label: 'Donald Trump',
        group: 'source',
        image: '/assets/img/nouser.png'
      },
      {
        id: 1,
        label: 'Barack Obama',
        group: 'person',
        image: '/asset/img/person/barack+obama.png'
      },
      {
        id: 2,
        label: 'Sean Spicer',
        group: 'person',
        image: '/asset/img/nouser.png'
      },
      {
        id: 3,
        label: 'Rex Tillerson',
        group: 'person',
        image: '/asset/img/nouser.png'
      },
      {
        id: 4,
        label: 'Mike Pence',
        group: 'person',
        image: '/asset/img/nouser.png'
      },
      {
        id: 5,
        label: 'Republican',
        group: 'organization',
        image: '/asset/img/organization/republican.png'
      },
      {
        id: 6,
        label: 'Reuters',
        group: 'organization',
        image: '/asset/img/noorg.png'
      },
      {
        id: 7,
        label: 'Senate',
        group: 'organization',
        image: '/asset/img/noorg.png'
      },
      {
        id: 8,
        label: 'Democratic',
        group: 'organization',
        image: '/asset/img/noorg.png'
      },
      {
        id: 9,
        label: 'European Union',
        group: 'organization',
        image: '/asset/img/noorg.png'
      },
      {
        id: 10,
        label: 'US',
        group: 'location',
        image: '/asset/img/nolocation.png'
      },
      {
        id: 11,
        label: 'United States',
        group: 'location',
        image: '/asset/img/nolocation.png'
      },
      {
        id: 12,
        label: 'White House',
        group: 'location',
        image: '/asset/img/nolocation.png'
      },
      {
        id: 13,
        label: 'U.S',
        group: 'location',
        image: '/asset/img/nolocation.png'
      },
      {
        id: 14,
        label: 'Washington',
        group: 'location',
        image: '/asset/img/nolocation.png'
      }
    ],
    edges: [
      {
        from: 0,
        value: 3,
        to: 1
      },
      {
        from: 0,
        value: 1,
        to: 2
      },
      {
        from: 0,
        value: 1,
        to: 3
      },
      {
        from: 0,
        value: 1,
        to: 4
      },
      {
        from: 0,
        value: 1,
        to: 5
      },
      {
        from: 0,
        value: 1,
        to: 6
      },
      {
        from: 0,
        value: 1,
        to: 7
      },
      {
        from: 0,
        value: 1,
        to: 8
      },
      {
        from: 0,
        value: 1,
        to: 9
      },
      {
        from: 0,
        value: 6,
        to: 10
      },
      {
        from: 0,
        value: 4,
        to: 11
      },
      {
        from: 0,
        value: 4,
        to: 12
      },
      {
        from: 0,
        value: 4,
        to: 13
      },
      {
        from: 0,
        value: 3,
        to: 14
      }
    ]
  };

  constructor() { }

  ngOnInit() {
    this.loadOntology(this.dataOntology);
  }

  ngOnChanges() {
    this.loadOntology(this.dataOntology);
  }

  loadOntology(dataOntology: any) {
    let nodes = [];
    let edges = [];
    if (dataOntology.node) {
      nodes = new vis.DataSet(dataOntology.node);
    } else {
      nodes = ([{ id: 0, label: 'No Data to Display', group: 'source', shape: 'text' }]);
    }

    if (dataOntology.edges) {
      edges = new vis.DataSet(dataOntology.edges);
    }

    // create a network
    let container = document.getElementById('mynetwork');

    // provide the data in the vis format
    let data = {
      nodes: nodes,
      edges: edges
    };

    let options = {
      edges: {
        width: 2,
        smooth: {
          type: 'continuous'
        }
      },
      nodes: {
        shape: 'dot',
        size: 20,
        font: {
          size: 15,
          color: '#000'
        },
        borderWidth: 2
      },
      groups: {
        person: {
          shape: 'icon',
          icon: {
            face: 'FontAwesome',
            code: '\uf007',
            size: 30,
            color: 'blue'
          }
        },
        location: {
          shape: 'icon',
          icon: {
            face: 'FontAwesome',
            code: '\uf041',
            size: 30,
            color: 'red'
          }
        },
        organization: {
          shape: 'icon',
          icon: {
            face: 'FontAwesome',
            code: '\uf0e8',
            size: 30,
            color: 'orange'
          }
        },
      }
    };

    let network = new vis.Network(container, data, options);
  }

}
