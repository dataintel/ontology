import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetOntologyComponent } from './src/ontology.component';

export * from './src/ontology.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetOntologyComponent,

  ],
  exports: [
    BaseWidgetOntologyComponent
  ]
})
export class OntologyModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: OntologyModule,
      providers: []
    };
  }
}
